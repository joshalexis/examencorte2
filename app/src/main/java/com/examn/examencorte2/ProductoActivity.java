package com.examn.examencorte2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import database.ListaProductos;
import database.Producto;

public class ProductoActivity extends AppCompatActivity {
    private EditText txtCodigoAct;
    private EditText txtNombreAct;
    private EditText txtMarcaAct;
    private EditText txtPrecioAct;
    private RadioButton rbPerecederoAct;
    private RadioButton rbNoPerecAct;
    private Button btnActualizar;
    private Button btnBuscar;
    private Button btnCerrar;
    private Button btnBorrar;
    private ListaProductos db;
    private Producto producto;
    private long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);
        txtCodigoAct = (EditText) findViewById(R.id.txtCodAct);
        txtNombreAct = (EditText) findViewById(R.id.txtNombreAct);
        txtMarcaAct = (EditText) findViewById(R.id.txtMarcaAct);
        txtPrecioAct = (EditText) findViewById(R.id.txtPrecioAct);
        rbPerecederoAct = (RadioButton) findViewById(R.id.rbPerecAct);
        rbNoPerecAct = (RadioButton) findViewById(R.id.rbNoPerecAct);
        btnActualizar = (Button) findViewById(R.id.btnActualizar);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        btnBorrar = (Button) findViewById(R.id.btnBorrar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        db = new ListaProductos(ProductoActivity.this);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtCodigoAct.getText().toString().equals("")) {
                    Toast.makeText(ProductoActivity.this, "Ingrese codigo de producto a buscar", Toast.LENGTH_SHORT).show();
                }else{
                    db.openDatabase();
                    int codigo = Integer.parseInt(txtCodigoAct.getText().toString());
                    producto = db.getProducto(codigo);
                    if(producto == null){
                        Toast.makeText(ProductoActivity.this,"El producto no existe",Toast.LENGTH_SHORT).show();
                    }else{
                        txtNombreAct.setText(producto.getNombre());
                        txtMarcaAct.setText(producto.getMarca());
                        txtPrecioAct.setText(String.valueOf(producto.getPrecio()));
                        if(producto.getPerecedero() == 1){
                            rbPerecederoAct.setChecked(true);
                        }else{
                            rbNoPerecAct.setChecked(true);
                        }
                    }
                }
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNombreAct.getText().toString().equals("") || txtCodigoAct.getText().toString().equals("")){
                    Toast.makeText(ProductoActivity.this,"Ingrese nombre producto o codigo",Toast.LENGTH_SHORT).show();
                }else{
                    db.openDatabase();
                    producto.setNombre(txtNombreAct.getText().toString());
                    producto.setMarca(txtMarcaAct.getText().toString());
                    Double precio = Double.valueOf(txtPrecioAct.getText().toString());
                    producto.setPrecio(precio);
                    if(rbPerecederoAct.isChecked()){
                        producto.setPerecedero(1);
                    }else{
                        producto.setPerecedero(0);
                    }
                    long id = db.updateProducto(producto,producto.getCodigo());
                    if(id > 0){
                        Toast.makeText(ProductoActivity.this,"Se actualizó producto con id: " + id,Toast.LENGTH_SHORT).show();
                        limpiar();
                        producto = null;
                        db.cerrar();
                    }else{
                        Toast.makeText(ProductoActivity.this,"No se actualizó producto - id " + id,Toast.LENGTH_SHORT).show();
                        limpiar();
                        producto = null;
                        db.cerrar();
                    }
                }
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtCodigoAct.getText().toString().equals("")){
                    Toast.makeText(ProductoActivity.this,"Ingrese codigo de producto",Toast.LENGTH_SHORT).show();
                }else{
                    db.openDatabase();
                    int codigo = Integer.parseInt(txtCodigoAct.getText().toString());
                    db.deleteProducto(codigo);
                    Toast.makeText(ProductoActivity.this,"Se ha eliminado el producto",Toast.LENGTH_SHORT).show();
                    limpiar();
                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void limpiar(){
        txtCodigoAct.setText("");
        txtNombreAct.setText("");
        txtMarcaAct.setText("");
        txtPrecioAct.setText("");
        rbPerecederoAct.setChecked(true);
        rbNoPerecAct.setChecked(false);
    }
}
