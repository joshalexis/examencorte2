package com.examn.examencorte2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import database.ListaProductos;
import database.Producto;

public class MainActivity extends AppCompatActivity {
    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioButton rbPerecedero;
    private RadioButton rbNoPerec;
    private Button btnGuardar;
    private Button btnEditar;
    private Button btnLimpiar;
    private Button btnNuevo;
    private ListaProductos db;
    private long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtCodigo = (EditText) findViewById(R.id.txtCodigo);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtMarca = (EditText) findViewById(R.id.txtMarca);
        txtPrecio = (EditText) findViewById(R.id.txtPrecio);
        rbPerecedero = (RadioButton) findViewById(R.id.rbPerecedero);
        rbNoPerec = (RadioButton) findViewById(R.id.rbNoPerec);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnEditar = (Button) findViewById(R.id.btnEditar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        db = new ListaProductos(MainActivity.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtCodigo.getText().toString().equals("") || txtNombre.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this,"Ingrese minimo Codigo y Nombre",Toast.LENGTH_SHORT).show();
                }else{
                    Producto p;
                    db.openDatabase();
                    p = db.getProducto(Integer.parseInt(txtCodigo.getText().toString()));
                    if(p != null){
                        Toast.makeText(MainActivity.this,"El produto ya existe",Toast.LENGTH_SHORT).show();
                    }else{
                        p = new Producto();
                        p.setCodigo(Integer.parseInt(txtCodigo.getText().toString()));
                        p.setNombre(txtNombre.getText().toString());
                        p.setMarca(txtMarca.getText().toString());
                        Double precio = Double.valueOf(txtPrecio.getText().toString());
                        p.setPrecio(precio);
                        if(rbPerecedero.isChecked()){
                            p.setPerecedero(1);
                        }else{
                            p.setPerecedero(0);
                        }
                        long id = db.insertProducto(p);
                        if(id > 0){
                            Toast.makeText(MainActivity.this,"Se agregó producto con id: " + id,Toast.LENGTH_SHORT).show();
                            limpiar();
                            db.cerrar();
                        }else{
                            Toast.makeText(MainActivity.this,"No se agregó producto, id: " + id,Toast.LENGTH_SHORT).show();
                            limpiar();
                            db.cerrar();
                        }
                    }
                }
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ProductoActivity.class);
                startActivity(intent);
                limpiar();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }

    private void limpiar(){
        txtCodigo.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
        rbPerecedero.setChecked(true);
        rbNoPerec.setChecked(false);
    }
}
