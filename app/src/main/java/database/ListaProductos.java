package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ListaProductos {
    private Context context;
    private ProductoDBHelper productoDBHelper;
    private SQLiteDatabase db;
    private String[] columnToRead = new String[]{
            DefinirTabla.Producto._ID,
            DefinirTabla.Producto.CODIGO,
            DefinirTabla.Producto.NOMBRE,
            DefinirTabla.Producto.MARCA,
            DefinirTabla.Producto.PRECIO,
            DefinirTabla.Producto.PERECEDERO
    };

    public ListaProductos(Context context){
        this.context = context;
        productoDBHelper = new ProductoDBHelper(context);
    }

    public void openDatabase(){
        db = productoDBHelper.getWritableDatabase();
    }

    public void cerrar(){
        productoDBHelper.close();
    }

    public long insertProducto(Producto p){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.CODIGO,p.getCodigo());
        values.put(DefinirTabla.Producto.NOMBRE,p.getNombre());
        values.put(DefinirTabla.Producto.MARCA,p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO,p.getPrecio());
        values.put(DefinirTabla.Producto.PERECEDERO,p.getPerecedero());
        return db.insert(DefinirTabla.Producto.TABLE_NAME,null,values);
    }

    public long updateProducto(Producto p, int codigo){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.CODIGO,p.getCodigo());
        values.put(DefinirTabla.Producto.NOMBRE,p.getNombre());
        values.put(DefinirTabla.Producto.MARCA,p.getMarca());
        values.put(DefinirTabla.Producto.PRECIO,p.getPrecio());
        values.put(DefinirTabla.Producto.PERECEDERO,p.getPerecedero());
        String criterio = DefinirTabla.Producto.CODIGO + " = " + codigo;
        return db.update(DefinirTabla.Producto.TABLE_NAME,values,criterio,null);
    }

    public int deleteProducto(int codigo){
        String criterio = DefinirTabla.Producto.CODIGO + " = " + codigo;;
        return db.delete(DefinirTabla.Producto.TABLE_NAME,criterio,null);
    }

    public Producto readProducto(Cursor cursor){
        Producto p = new Producto();
        p.set_ID(cursor.getInt(0));
        p.setCodigo(cursor.getInt(1));
        p.setNombre(cursor.getString(2));
        p.setMarca(cursor.getString(3));
        p.setPrecio(cursor.getDouble(4));
        p.setPerecedero(cursor.getInt(5));
        return p;
    }

    public Producto getProducto(int codigo){
        Producto p = null;
        SQLiteDatabase db = productoDBHelper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.Producto.TABLE_NAME,columnToRead,
                DefinirTabla.Producto.CODIGO + " = ? ",
                new String[]{String.valueOf(codigo)},null,null,null);
        if(!cursor.moveToFirst()){
            return p;
        }
        p = readProducto(cursor);
        cursor.close();
        return p;
    }
}
